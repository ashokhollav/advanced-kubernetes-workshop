cd /tmp
git clone https://gitlab.com/ameer00/advanced-kubernetes-workshop.git 

export GCP_REGION=us-central1
export GCP_ZONE_1=us-central1-f
export GCP_ZONE_2=us-east1-b

gcloud container clusters get-credentials gke-central --zone $GCP_ZONE_1 --project $(gcloud info --format='value(config.project)')
gcloud container clusters get-credentials gke-east --zone $GCP_ZONE_2 --project $(gcloud info --format='value(config.project)')
gcloud container clusters get-credentials gke-spinnaker --zone $GCP_ZONE_1 --project $(gcloud info --format='value(config.project)')

kubectx gke-central="gke_"$(gcloud info --format='value(config.project)')"_"$GCP_ZONE_1"_gke-central"
kubectx gke-east="gke_"$(gcloud info --format='value(config.project)')"_"$GCP_ZONE_2"_gke-east"
kubectx gke-spinnaker="gke_"$(gcloud info --format='value(config.project)')"_"$GCP_ZONE_1"_gke-spinnaker"


<<'COMMENT'
#Install Istio on gke-central and gke-east
cd /tmp
curl -L https://git.io/getLatestIstio | sh -
cd istio-0.8.0

kubectx gke-central
kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account)
kubectl create serviceaccount tiller --namespace kube-system
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account=tiller

kubectx gke-east
kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account)
kubectl create serviceaccount tiller --namespace kube-system
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account=tiller

kubectx gke-spinnaker
kubectl create clusterrolebinding user-admin-binding --clusterrole=cluster-admin --user=$(gcloud config get-value account)
kubectl create serviceaccount tiller --namespace kube-system
kubectl create clusterrolebinding tiller-admin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account=tiller

sleep 30
kubectx gke-central
helm install --namespace=istio-system --set sidecar-injector.enabled=true install/kubernetes/helm/istio

kubectx gke-east
helm install --namespace=istio-system --set sidecar-injector.enabled=true install/kubernetes/helm/istio

kubectl label namespace default istio-injection=enabled --context gke-central
kubectl label namespace default istio-injection=enabled --context gke-east

#Install Spinnaker on gke-spinnaker
cd /tmp/advanced-kubernetes-workshop/
kubectx gke-spinnaker

gcloud iam service-accounts create spinnaker-sa --display-name spinnaker-sa
export SPINNAKER_SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-sa" \
    --format='value(email)')
export PROJECT=$(gcloud info --format='value(config.project)')
gcloud projects add-iam-policy-binding $PROJECT --role roles/storage.admin --member serviceAccount:$SPINNAKER_SA_EMAIL
gcloud iam service-accounts keys create spinnaker-key.json --iam-account $SPINNAKER_SA_EMAIL

export BUCKET=$PROJECT-spinnaker-conf
gsutil mb -c regional -l $GCP_REGION gs://$BUCKET

git clone https://github.com/viglesiasce/charts -b mcs
cd charts/stable/spinnaker
helm dep build

kubectl create clusterrolebinding client-cluster-admin-binding --clusterrole=cluster-admin --user=client --context gke-central
kubectl create clusterrolebinding client-cluster-admin-binding --clusterrole=cluster-admin --user=client --context gke-east
CLOUDSDK_CONTAINER_USE_CLIENT_CERTIFICATE=True gcloud container clusters get-credentials gke-central --zone $GCP_ZONE_1
CLOUDSDK_CONTAINER_USE_CLIENT_CERTIFICATE=True gcloud container clusters get-credentials gke-east --zone $GCP_ZONE_2

kubectx gke-spinnaker
kubectl create secret generic --from-file=config=$HOME/.kube/config my-kubeconfig

sed -i s/ClusterIP/LoadBalancer/g $DIR/advanced-kubernetes-workshop/charts/stable/spinnaker/values.yaml

cd /tmp/advanced-kubernetes-workshop/
SA_JSON=$(cat spinnaker-key.json)
export SA_JSON
export BUCKET=$PROJECT-spinnaker-conf
export CL1_CONTEXT="gke_"$PROJECT"_"$GCP_ZONE_1"_gke-central"
export CL2_CONTEXT="gke_"$PROJECT"_"$GCP_ZONE_2"_gke-east"

cd charts/stable/spinnaker
cat > spinnaker-config.yaml <<EOF
storageBucket: $BUCKET
kubeConfig:
  enabled: true
  secretName: my-kubeconfig
  secretKey: config
  contexts:
  - $CL1_CONTEXT
  - $CL2_CONTEXT
gcs:
  enabled: true
  project: $PROJECT
  jsonKey: '$SA_JSON'

# Disable minio the default
minio:
  enabled: false

# Disable jenkins
jenkins:
  enabled: false

# Configure your Docker registries here
accounts:
- name: gcr
  address: https://gcr.io
  username: _json_key
  password: '$SA_JSON'
  email: 1234@5678.com 
EOF

kubectx gke-spinnaker
helm install -n spinnaker . -f spinnaker-config.yaml
COMMENT
